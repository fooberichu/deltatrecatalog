﻿using System;
using System.Collections.Generic;
using System.Text;
using DeltatreCatalog.Logic.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Microsoft.Extensions.Caching.Memory;
using DeltatreCatalog.Logic.Threading;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Model;
using DeltatreCatalog.Logic.Tests.Helpers;

namespace DeltatreCatalog.Logic.Tests.Products
{
	[TestClass]
	public class ProductRepositoryTests : BaseTest
	{
		private IMemoryCache Cache;

		private IProductRepository Target;

		[TestInitialize]
		public void Initialize()
		{
			Cache = Substitute.For<IMemoryCache>();

			Services.AddSingleton(Cache);
			Services.AddSingleton<InMemoryProductRepository>();

			Target = Provider.GetService<InMemoryProductRepository>();
		}

		[TestMethod]
		public void ProductRepository_GetById_Success()
		{
			// arrange
			int productId = Random.Next();
			var products = new[] {
				FakesProductHelper
					.CreateProduct()
					.WithProductId(productId)
			};

			Product[] retProducts;
			Cache
				.TryGetValue(CacheConstants.ProductCacheKey, out retProducts)
				.ReturnsForAnyArgs(x =>
				{
					x[1] = products;
					return true;
				});

			// act
			var result = AsyncHelper.RunSync(() => Target.GetAsync(productId));

			// assert
			Assert.IsNotNull(result);
			Assert.AreEqual(productId, result?.ProductId);
			Assert.Fail();
		}
	}
}

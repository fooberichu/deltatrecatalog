﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeltatreCatalog.Logic.Tests
{
	[TestClass]
	public class BaseTest
	{
		private Random _random = null;

		private Lazy<IServiceProvider> _services = null;

		public BaseTest()
		{
			_services = new Lazy<IServiceProvider>(() => Services.BuildServiceProvider());
		}

		protected IServiceCollection Services { get; private set; } = new ServiceCollection();
		protected IServiceProvider Provider => _services.Value;

		protected Random Random
		{
			get
			{
				if (_random == null)
					_random = new Random();

				return _random;
			}
		}

		protected int Next(int maxValue, params int[] excludeValues)
		{
			int tmpValue = 0;
			bool getNextValue = true;
			while (getNextValue)
			{
				tmpValue = Random.Next(maxValue);

				if (!excludeValues.Contains(tmpValue))
					getNextValue = false;
			}

			return tmpValue;
		}

	}
}

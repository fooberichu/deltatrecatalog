﻿using System;
using System.Collections.Generic;
using System.Text;
using DeltatreCatalog.Model;

namespace DeltatreCatalog.Logic.Tests.Helpers
{
	public static class FakesProductHelper
	{
		public static Product CreateProduct()
		{
			return new Product();
		}

		public static Product WithProductId(this Product product, int productId)
		{
			product.ProductId = productId;

			return product;
		}
	}
}

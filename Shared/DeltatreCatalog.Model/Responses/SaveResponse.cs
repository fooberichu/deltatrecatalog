﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Responses
{
	public class SaveResponse<T> : ResponseBase where T : class
	{
		public T SavedItem { get; set; }
	}
}

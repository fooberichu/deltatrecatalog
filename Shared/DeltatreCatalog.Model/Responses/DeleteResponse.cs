﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Responses
{
	//public interface IDeletedItemResponse<T>
	//{
	//	T DeletedItem { get; }
	//}

	public class DeleteResponse<T> : ResponseBase where T : struct
	{
		public T? DeletedItemId { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Responses
{
	public interface IValidationResponse
	{
		string Message { get; set; }
		ValidationResponse Validation { get; set; }
	}

	public class ValidationResponse
	{
		public bool IsValid { get; set; }
		public ValidationResult[] Results { get; set; }
	}

	public class ValidationResult
	{
		public string Property { get; set; }
		public string Message { get; set; }
		public string[] MemberNames { get; set; }
		public string ReferenceId { get; set; }
	}
}

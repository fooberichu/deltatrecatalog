﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Responses
{
	public class ResponseBase : IValidationResponse
	{
		public bool Success { get; set; }
		public string Message { get; set; }
		public ValidationResponse Validation { get; set; }
	}
}

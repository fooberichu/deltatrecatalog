﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Requests.Products
{
	public class ProductRequest
	{
		public int? ProductId { get; set; }

		public string Name { get; set; }
	}
}

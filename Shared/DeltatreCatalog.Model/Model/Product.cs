﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeltatreCatalog.Model
{
	public class Product
	{
		public int? ProductId { get; set; }

		[Required]
		public string Name { get; set; }
		public string Description { get; set; }
		// TODO: in a real situation this would be a calculated field based on a "stock history" handling (intake, adjustments, and shipments)
		public int QuantityOnHand { get; set; }
	}
}

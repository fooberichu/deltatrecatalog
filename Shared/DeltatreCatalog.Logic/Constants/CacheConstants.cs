﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Logic.Constants
{
	public static class CacheConstants
	{
		public const string ProductCacheKey = "_Products";
		public const string NextProductIdKey = "_NextProductId";

		public const int InitialProductCount = 10;
		public const int MinimumQuantityOnHand = 1;
		public const int MaximumQuantityOnHand = 10;
	}
}

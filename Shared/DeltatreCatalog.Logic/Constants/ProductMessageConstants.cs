﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Logic.Constants
{
	public static class ProductMessageConstants
	{
		public const string Product_Error_CannotPostWithId = "Cannot POST new product with existing ID";
		public const string Product_Error_ExistingName = "Product already exists with this name.";
		public const string Product_Error_FailedToDelete = "Failed to delete product.";

		public const string Product_Message_FailedValidation = "Product failed validation.";
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeltatreCatalog.Logic.Constants
{
	public static class ProductsHubConstants
	{
		public const string ProductAdded = "ProductAdded";
		public const string ProductUpdated = "ProductUpdated";
	}
}

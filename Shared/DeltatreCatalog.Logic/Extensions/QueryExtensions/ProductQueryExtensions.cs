﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeltatreCatalog.Model;

namespace DeltatreCatalog.Logic.Extensions.QueryExtensions
{
	public static class ProductQueryExtensions
	{
		public static IQueryable<Product> ById(this IQueryable<Product> query, int? productId)
		{
			if (productId.HasValue)
				query = query.Where(product => product.ProductId == productId);

			return query;
		}

		public static IQueryable<Product> ByName(this IQueryable<Product> query, string name)
		{
			// TODO: if this were on a *real* EF query it wouldn't work... would need to solve slightly different
			// on that same token, it would likely be a partial search not a full name search
			if (!string.IsNullOrWhiteSpace(name))
				query = query.Where(product => string.Compare(product.Name, name, true) == 0);

			return query;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DeltatreCatalog.Model;

namespace DeltatreCatalog.Logic.Extensions.ModelExtensions
{
	public static class ProductExtensions
	{
		public static bool IsNew(this Product product)
		{
			return !product.ProductId.HasValue || product.ProductId == 0;
		}

		// ideally this would be assigning values into an EF entity but in this case, just copying properties from a "source" model
		public static void FromModel(this Product product, Product model)
		{
			product.Description = model.Description;
			product.Name = model.Name;
			product.QuantityOnHand = model.QuantityOnHand;
		}
	}
}

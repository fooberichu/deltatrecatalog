﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeltatreCatalog.Responses;
using MsAnnotations = System.ComponentModel.DataAnnotations;

namespace DeltatreCatalog.Logic.Validators
{
	public interface IDataAnnotationValidator
	{
		bool TryValidate(object objectToValidate, out ValidationResponse validationResponse);
		bool TryValidate(object objectToValidate, bool validateAllProperties, out ValidationResponse validationResponse);
	}

	public class DataAnnotationValidator : IDataAnnotationValidator
	{
		public bool TryValidate(object objectToValidate, out ValidationResponse validationResponse)
		{
			return TryValidate(objectToValidate, true, out validationResponse);
		}

		public bool TryValidate(object objectToValidate, bool validateAllProperties, out ValidationResponse validationResponse)
		{
			var context = new MsAnnotations.ValidationContext(objectToValidate, null, null);
			var results = new List<MsAnnotations.ValidationResult>();

			bool isValid = MsAnnotations.Validator.TryValidateObject(objectToValidate, context, results, validateAllProperties);

			validationResponse = new ValidationResponse
			{
				IsValid = isValid,
				Results = results
					.Select(Convert)
					.ToArray()
			};

			return isValid;
		}

		private ValidationResult Convert(MsAnnotations.ValidationResult coreResult)
		{
			var result = new ValidationResult
			{
				Message = coreResult.ErrorMessage,
				Property = coreResult.MemberNames.FirstOrDefault(),
				MemberNames = coreResult.MemberNames.ToArray()
			};

			return result;
		}
	}
}

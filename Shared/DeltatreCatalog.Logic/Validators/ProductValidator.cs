﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Logic.Extensions.ModelExtensions;
using DeltatreCatalog.Logic.Repositories;
using DeltatreCatalog.Logic.Threading;
using DeltatreCatalog.Model;
using DeltatreCatalog.Requests.Products;
using DeltatreCatalog.Responses;
using MsAnnotations = System.ComponentModel.DataAnnotations;

namespace DeltatreCatalog.Logic.Validators
{
	public interface IProductValidator
	{
		void Initialize(Product product);
		Task<ValidationResponse> Validate();
	}

	public class ProductValidator : IProductValidator
	{
		public ProductValidator(IProductRepository productRepository, 
			IDataAnnotationValidator dataAnnotationValidator)
		{
			ProductRepository = productRepository;
			DataValidator = dataAnnotationValidator;
		}

		protected IProductRepository ProductRepository { get; }
		protected IDataAnnotationValidator DataValidator { get; }

		protected Product Product { get; set; }

		public void Initialize(Product product)
		{
			Product = product;
		}

		public async Task<ValidationResponse> Validate()
		{
			List<ValidationResult> validationResults = new List<ValidationResult>();

			if (!DataValidator.TryValidate(Product, out ValidationResponse validationResponse))
				return validationResponse;

			if (Product.IsNew())
			{
				var existingProduct = (await ProductRepository.GetAsync(new ProductRequest { Name = Product.Name })).FirstOrDefault();
				if (existingProduct != null)
				{
					validationResults.Add(new ValidationResult
					{
						Message = ProductMessageConstants.Product_Error_ExistingName,
						Property = nameof(Product.Name),
						ReferenceId = Product.ProductId.ToString()
					});
				}
			}

			return new ValidationResponse
			{
				IsValid = validationResults.Count == 0,
				Results = validationResults.ToArray()
			};
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DeltatreCatalog.Logic.Handlers;
using DeltatreCatalog.Logic.Hydrators;
using DeltatreCatalog.Logic.Repositories;
using DeltatreCatalog.Logic.Validators;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeltatreCatalog.Logic.Configuration
{
	public static class AssemblyInitializer
	{
		public static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
		{
			services
				.RegisterProducts()
				.RegisterCoreValidators();

			return services;
		}

		public static IServiceProvider HydrateCache(this IServiceProvider services)
		{
			using (var scope = services.CreateScope())
			{
				var hydrator = scope.ServiceProvider.GetService<IProductCacheHydrator>();
				hydrator.Hydrate();
			}

			return services;
		}

		private static IServiceCollection RegisterProducts(this IServiceCollection services)
		{
			services.AddTransient<IProductRepository, InMemoryProductRepository>();
			services.AddTransient<IProductCacheHydrator, ProductCacheHydrator>();
			services.AddTransient<IProductValidator, ProductValidator>();

			services.AddTransient<IDeleteProductHandler, DeleteProductHandler>();
			services.AddTransient<IGetProductsHandler, GetProductsHandler>();
			services.AddTransient<IUpdateProductHandler, UpdateProductHandler>();

			return services;
		}

		private static IServiceCollection RegisterCoreValidators(this IServiceCollection services)
		{
			services.AddTransient<IDataAnnotationValidator, DataAnnotationValidator>();

			return services;
		}
	}
}

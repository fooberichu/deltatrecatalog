﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Logic.Repositories;
using DeltatreCatalog.Responses;

namespace DeltatreCatalog.Logic.Handlers
{
	public interface IDeleteProductHandler
	{
		Task<DeleteResponse<int>> DeleteAsync(int productId);
	}

	public class DeleteProductHandler : IDeleteProductHandler
	{
		public DeleteProductHandler(IProductRepository productRepository)
		{
			ProductRepository = productRepository;
		}

		protected IProductRepository ProductRepository { get; }

		public async Task<DeleteResponse<int>> DeleteAsync(int productId)
		{
			var deleteResponse = new DeleteResponse<int>();

			try
			{
				deleteResponse.DeletedItemId = await ProductRepository.DeleteAsync(productId);
				deleteResponse.Success = true;
			}
			catch
			{
				deleteResponse.Message = ProductMessageConstants.Product_Error_FailedToDelete;
			}

			return deleteResponse;
		}
	}
}
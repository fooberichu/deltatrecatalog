﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Repositories;
using DeltatreCatalog.Model;
using DeltatreCatalog.Requests.Products;

namespace DeltatreCatalog.Logic.Handlers
{
	public interface IGetProductsHandler
	{
		Task<Product> GetAsync(int productId);
		Task<IEnumerable<Product>> GetAsync(ProductRequest request);
	}

	// realistically this would handle conversion from the data to an appropriate model for the outside
	public class GetProductsHandler : IGetProductsHandler
	{
		public GetProductsHandler(IProductRepository productRepository)
		{
			ProductRepository = productRepository;
		}

		protected IProductRepository ProductRepository { get; }

		public Task<Product> GetAsync(int productId)
		{
			return ProductRepository.GetAsync(productId);
		}

		public Task<IEnumerable<Product>> GetAsync(ProductRequest request)
		{
			return ProductRepository.GetAsync(request);
		}
	}
}

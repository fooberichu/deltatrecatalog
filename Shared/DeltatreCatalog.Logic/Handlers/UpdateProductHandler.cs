﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Logic.Extensions.ModelExtensions;
using DeltatreCatalog.Logic.Hubs;
using DeltatreCatalog.Logic.Repositories;
using DeltatreCatalog.Logic.Validators;
using DeltatreCatalog.Model;
using DeltatreCatalog.Responses;
using Microsoft.AspNetCore.SignalR;

namespace DeltatreCatalog.Logic.Handlers
{
	public interface IUpdateProductHandler
	{
		void Initialize(Product product);
		Task<SaveResponse<Product>> ProcessUpdateRequestAsync();
	}

	// this could be broken out into two handlers: one for add, one for update... for SOLID principles it probably even should
	public class UpdateProductHandler : IUpdateProductHandler
	{
		public UpdateProductHandler(IProductRepository productRepository,
			IProductValidator productValidator,
			IHubContext<ProductsHub> hubContext)
		{
			ProductRepository = productRepository;
			ProductValidator = productValidator;
			HubContext = hubContext;
		}

		protected IProductRepository ProductRepository { get; }
		protected IProductValidator ProductValidator { get; }
		protected IHubContext<ProductsHub> HubContext { get; }

		protected Product Product { get; set; }

		public void Initialize(Product product)
		{
			Product = product;
			ProductValidator.Initialize(Product);
		}

		public async Task<SaveResponse<Product>> ProcessUpdateRequestAsync()
		{
			var saveResponse = new SaveResponse<Product>();

			saveResponse.Validation = await ProductValidator.Validate();
			if (saveResponse.Validation.IsValid)
			{
				Product updatedProduct = null;

				if (Product.IsNew())
					updatedProduct = await ProductRepository.CreateAsync(Product);
				else
					updatedProduct = await ProductRepository.UpdateAsync(Product);

				saveResponse.SavedItem = updatedProduct;
				saveResponse.Success = true;

				await HubContext.Clients.All.SendAsync(ProductsHubConstants.ProductUpdated, updatedProduct, Product.IsNew());
			}
			else
				saveResponse.Message = ProductMessageConstants.Product_Message_FailedValidation;

			return saveResponse;
		}
	}
}
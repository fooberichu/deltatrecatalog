﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeltatreCatalog.Model;
using Microsoft.AspNetCore.SignalR;

namespace DeltatreCatalog.Logic.Hubs
{
	public class ProductsHub : Hub
	{
		public ProductsHub()
		{
		}

		public override Task OnConnectedAsync()
		{
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			return base.OnDisconnectedAsync(exception);
		}
	}
}

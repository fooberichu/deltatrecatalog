﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Model;
using DeltatreCatalog.Requests.Products;
using DeltatreCatalog.Logic.Extensions.QueryExtensions;
using Microsoft.Extensions.Caching.Memory;
using DeltatreCatalog.Logic.Extensions.ModelExtensions;

namespace DeltatreCatalog.Logic.Repositories
{
	public interface IProductRepository
	{
		Task<Product> CreateAsync(Product newProduct);
		Task<int> DeleteAsync(int productId);
		Task<Product> GetAsync(int productId);
		Task<IEnumerable<Product>> GetAsync(ProductRequest request);
		Task<Product> UpdateAsync(Product product);
	}

	public class InMemoryProductRepository : IProductRepository
	{
		public InMemoryProductRepository(IMemoryCache memoryCache)
		{
			MemoryCache = memoryCache;
		}

		protected IMemoryCache MemoryCache { get; }

		public Task<Product> GetAsync(int productId)
		{
			return Task.FromResult(GetCachedProducts()
				.FirstOrDefault(product => product.ProductId == productId));
		}

		public Task<IEnumerable<Product>> GetAsync(ProductRequest request)
		{
			return Task.FromResult(GetCachedProducts()
				.AsQueryable()
				.ById(request.ProductId)
				.ByName(request.Name)
				.AsEnumerable());
		}

		public Task<int> DeleteAsync(int productId)
		{
			var products = GetCachedProducts();

			var dataProduct = products
				.AsQueryable()
				.ById(productId)
				.Single();

			UpdateCachedProducts(products.Except(new[] { dataProduct }).ToArray());

			return Task.FromResult(dataProduct.ProductId.Value);
		}

		public Task<Product> CreateAsync(Product newProduct)
		{
			EnsureUniqueProduct(newProduct);

			newProduct.ProductId = NextProductId();
			UpdateCachedProducts(GetCachedProducts().Concat(new[] { newProduct }).ToArray());

			return Task.FromResult(newProduct);
		}

		public Task<Product> UpdateAsync(Product product)
		{
			var products = GetCachedProducts();
			EnsureUniqueProduct(product);

			var existingProduct = products
				.AsQueryable()
				.ById(product.ProductId)
				.SingleOrDefault();

			existingProduct.FromModel(product);

			return Task.FromResult(existingProduct);
		}

		private Product[] GetCachedProducts()
		{
			return MemoryCache.TryGetValue(CacheConstants.ProductCacheKey, out Product[] products)
				? products
				: Array.Empty<Product>();
		}

		private void EnsureUniqueProduct(Product product)
		{
			var products = GetCachedProducts();

			var matchingProduct = products
				.AsQueryable()
				.ByName(product.Name)
				.SingleOrDefault();

			if (matchingProduct != null && matchingProduct.ProductId != product.ProductId)
			{
				throw new ArgumentException(ProductMessageConstants.Product_Error_ExistingName, nameof(Product.Name));
			}
		}

		private int NextProductId()
		{
			if (MemoryCache.TryGetValue(CacheConstants.NextProductIdKey, out int nextProductId))
			{
				MemoryCache.Set(CacheConstants.NextProductIdKey, nextProductId + 1);
				return nextProductId;
			}

			return -1;
		}

		private void UpdateCachedProducts(Product[] products)
		{
			MemoryCache.Set(CacheConstants.ProductCacheKey, products);
		}
	}
}

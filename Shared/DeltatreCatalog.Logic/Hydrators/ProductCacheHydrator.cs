﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Model;
using Microsoft.Extensions.Caching.Memory;

namespace DeltatreCatalog.Logic.Hydrators
{
	public interface IProductCacheHydrator
	{
		void Hydrate();
	}

	/// <summary>
	/// Intended to be called from DeltatreCatalog/Program.cs:Main
	/// </summary>
	public class ProductCacheHydrator : IProductCacheHydrator
	{
		public ProductCacheHydrator(IMemoryCache cache)
		{
			Cache = cache;
		}

		protected IMemoryCache Cache { get; }

		public void Hydrate()
		{
			List<Product> products = new List<Product>();
			Random random = new Random();

			for (int productIndex = 0; productIndex < CacheConstants.InitialProductCount; productIndex++)
			{
				int productId = productIndex + 1;
				products.Add(new Product
				{
					Description = $"Description for product {productId}",
					Name = $"Product {productId}",
					ProductId = productId,
					QuantityOnHand = random.Next(CacheConstants.MinimumQuantityOnHand, CacheConstants.MaximumQuantityOnHand + 1)
				});
			}

			Cache.Set(CacheConstants.ProductCacheKey, products.ToArray());
			Cache.Set(CacheConstants.NextProductIdKey, (products.Last().ProductId + 1));
		}
	}
}

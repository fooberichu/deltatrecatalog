import qs from 'qs';
import { isNullOrUndefined, requestBase, handleResponse } from '../../_helpers';

const apiBase = 'api/products';

class ProductService {
	constructor() { }

	filter() {
		let getRequest = {
		};

		let url = `${apiBase}/?${qs.stringify(getRequest)}`;
		let request = Object.assign({}, requestBase, { method: 'GET' });

		return fetch(url, request)
			.then(handleResponse);
	}

	get(productId) {
		let url = `${apiBase}/${productId}`;
		let request = Object.assign({}, requestBase, { method: 'GET' });

		return fetch(url, request)
			.then(handleResponse);
	}

	delete(productId) {
		let url = `${apiBase}/${productId}`;
		let request = Object.assign({}, requestBase, { method: 'DELETE' });

		return fetch(url, request)
			.then(handleResponse);
	}

	save(product) {
		// TODO: Jon - Intranet - validate javascript
		let method = isNullOrUndefined(product.productId) ? 'POST' : 'PUT';
		let request = Object.assign({}, requestBase, { method, body: JSON.stringify(product) });
		let url = method === 'POST' ? `${apiBase}` : `${apiBase}/${product.productId}`;

		return fetch(url, request)
			.then(handleResponse);
	}
}

const instance = Object.freeze(new ProductService());
export { instance as ProductService }
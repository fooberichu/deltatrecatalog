import * as React from 'react';
import { toast } from 'react-toastify';

// const defaultOptions = {
//     type: 'info'
// }

// const errorDefaultOptions = {
//     type: 'error'
// }

// const successDefaultOptions = {
//     type: 'success'
// }

// https://www.npmjs.com/package/react-toastify
class NotificationService {
    constructor() {

    }

    success(message) {
        this.message(message, { type: 'success' });
        // toast.success(message);
    }

    error(message) {
        this.message(message, { type: 'error' });
    }

    info(message) {
        this.message(message, { type: 'info' });
    }

    message(message, opts = null) {
        const { type } = opts;
        let content = message;
        if (typeof message === 'string')
            content = <p className={`text-white`} dangerouslySetInnerHTML={{ __html: message }} />;

        toast(content, opts);
    }
}

const instance = Object.freeze(new NotificationService());
export { instance as NotificationService }
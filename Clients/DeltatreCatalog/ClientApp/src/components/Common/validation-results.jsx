import React from 'react';

export const ValidationResultItem = (props) => {
	const { className, property, message, memberNames } = props;

	return (
		<div className={`validation-result alert-danger p-2 ${className || ''}`}>
			{message}
		</div>
	);
}

export const ValidationResults = (props) => {
	const { className, results, children } = props;

	// _.uniqueId()
	const resultItems = results.map((r, index) => <ValidationResultItem
		className={index > 0 ? 'mt-2' : null}
		key={`vr-${r.property}`}
		property={r.property}
		message={r.message}
		memberNames={r.memberNames}
	/>);

	return (
		<div className={`validation-results ${className || ''}`}>
			{resultItems}
		</div>
	);
}
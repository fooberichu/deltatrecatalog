import React, { Component } from 'react';

import { ValidatorComponent } from 'react-form-validator-core';

export class TextRequiredValidator extends ValidatorComponent {
	render() {
		const { className, errorMessages, validators, requiredError, validatorListener, ...rest } = this.props;

		return (
			<div>
				<input
					className={`${className}${(this.state.isValid ? '' : ' border-danger')}`}
					{...rest}
					ref={(r) => { this.input = r; }}
				/>
				{this.errorText()}
			</div>
		);
	}

	errorText() {
		const { isValid } = this.state;

		if (isValid) {
			return null;
		}

		return (
			<div className="text-danger">
				{this.getErrorMessage()}
			</div>
		);
	}
}

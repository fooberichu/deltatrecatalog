import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logger from 'debug';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr';

import { ProductService } from '../../_services';
import { ProductListItem } from './product-list-item';
import { isNullOrUndefined } from '../../_helpers';

const debug = logger('products');
export class ProductListing extends Component {
	static displayName = ProductListing.name;

	constructor(props) {
		super(props);

		this.state = {
			filter: {},
			products: null,
			loading: true,
			hubConnection: null
		};

		this.connection = new HubConnectionBuilder()
			.withUrl('/productshub')
			.configureLogging(LogLevel.Information)
			.build();

		this.onDeleteRequested = this.onDeleteRequested.bind(this);
	}

	componentDidMount() {
		this.filter();

		this.connection
			.start({ withCredentials: false })
			.catch(err => console.error(err.toString()));

		this.connection.on('ProductUpdated', (product) => {
			debug('productUpdated: ', product);

			let { products } = this.state;
			let existingProduct = products.find(p => p.productId === product.productId);
			if (isNullOrUndefined(existingProduct)) {
				products.push(product);
			}
			else {
				Object.assign(existingProduct, product);
			}
			
			this.setState({ products });
		});
	}

	componentWillUnmount() {
		this.connection.stop();
	}

	filter() {
		ProductService.filter()
			.then(response => {
				debug('response', response);
				this.setState({ products: response, loading: false })
			})
			.catch(err => this.setState({ loading: false }));
	}

	onDeleteRequested({ productId }) {
		ProductService.delete(productId).then(response => {
			if (response.success && response.deletedItemId === productId) {
				let { products } = this.state;
				products = products.filter(val => val.productId !== productId);

				this.setState({ products });
			}
		});
	}

	renderList() {

		if (this.state.products.length === 0) {
			return (
				<div>
					<p>There are no products available.</p>
					<Link className="btn btn-primary mr-2" to='/products/new' title="New Product">Add new Product</Link>
				</div>
			);
		}

		let header = (
			<div className="product-header">
				<div>
					<div className="product-column"></div>
					<div className="product-column">Name</div>
					<div className="product-column">Description</div>
					<div className="product-column">QoH</div>
				</div>
			</div>
		);
		let rows = this.state.products.map(item => <ProductListItem
			key={item.productId}
			item={item}
			deleteRequested={this.onDeleteRequested}
		/>);
		let body = (<div className="product-body">{rows}</div>);

		return (
			<div className="product-listing">
				{header}
				{body}
			</div>
		);
	}

	render() {
		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: (
				<div>
					{this.renderList()}
				</div>
			);

		return (
			<div>
				<h2>Products</h2>
				{contents}
			</div>
		);
	}
}
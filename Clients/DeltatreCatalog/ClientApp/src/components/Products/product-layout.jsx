import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Products.scss';

export class ProductLayout extends Component {
	static displayName = ProductLayout.name;

	render() {
		return (
			<div className="products">
				<div className="bg-dark px-3 py-2">
					<Link className="btn btn-light mr-2" to='/products' title="Product Listing"><FontAwesomeIcon icon={['fas', 'boxes']} size="lg" /></Link>
					<Link className="btn btn-light mr-2" to='/products/new' title="New Product"><FontAwesomeIcon icon={'plus'} size="lg" /></Link>
				</div>
				<div className="p-3">
					{this.props.children}
				</div>
			</div>
		);
	}
}
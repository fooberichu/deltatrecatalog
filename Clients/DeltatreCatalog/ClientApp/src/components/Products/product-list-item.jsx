import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logger from 'debug';
import truncate from 'truncate';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DelayedActionButton } from '../Common/delayed-action-button';

const debug = logger('products');
export class ProductListItem extends Component {
	static displayName = ProductListItem.name;

	onEditClicked(event) {
		const { productId } = this.props.item;
		debug('onEditClicked', productId);

		this.props.editRequested(productId);
	}

	onDeleteClicked(event) {
		const { productId } = this.props.item;
		debug('onDeleteClicked', productId);

		this.props.deleteRequested({ productId });
	}

	render() {
		const { productId, name, description, quantityOnHand } = this.props.item;

		const shortened = truncate(description, 50);
		return (
			<div className="product-list-item border border-dark border-md-none rounded p-2 p-md-0 mb-2 mb-md-0">
				<div className="product-column d-flex align-items-center">
					<Link className="btn bare" to={`/products/${productId}`}><FontAwesomeIcon icon="pencil-alt" /></Link>
					<DelayedActionButton classNames={{ button: 'btn bare' }} action={this.onDeleteClicked.bind(this)}><FontAwesomeIcon icon="trash" /></DelayedActionButton>
				</div>
				<div className="product-column"><label className="d-md-none">Name:</label>{name}</div>
				<div className="product-column"><label className="d-md-none">Description:</label>{shortened}</div>
				<div className="product-column"><label className="d-md-none">Quantity on Hand:</label>{quantityOnHand}</div>
			</div>
		);
	}
}
import React, { Component } from 'react';
// https://stackoverflow.com/a/42124328/1130952
import { withRouter } from 'react-router-dom'

import logger from 'debug';

import { NotificationService, ProductService } from '../../_services';
import { isValid, isNullOrEmpty } from '../../_helpers';

import { ValidatorForm } from 'react-form-validator-core';

import { ProductDetail } from './product-detail';
import { productModel } from './product-models';

import { ValidationResults } from '../Common/validation-results';

const debug = logger('products');
class Product extends Component {
	static displayName = Product.name;

	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			product: null,
			validationResults: null,
			formErrors: { name: { isValid: false, message: '' } }
		};

		this.onProductChanged = this.onProductChanged.bind(this);
		this.onCancel = this.onCancel.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentDidMount() {
		const { match } = this.props;
		const { productId } = match.params;
		if (!isNaN(productId)) {
			// we need to load the item...
			ProductService
				.get(productId)
				.then(response => {
					debug('loaded', response);
					this.setState({
						product: response,
						loading: false
					});
				});
		}
		else {
			this.setState({
				loading: false,
				product: Object.assign({}, productModel)
			});
		}
	}

	onProductChanged(product) {
		this.setState({ product });
	}

	onSubmit(event) {
		const { history } = this.props;

		event.preventDefault();
		event.stopPropagation();
		debug('submit');

		let product = this.state.product;

		ProductService.save(product)
			.then(response => {
				debug('save response:', response);
				if (response.success) {
					NotificationService.success(`Product: '${this.state.product.name}' saved.`);
					setTimeout(() => history.push('/products'), 750);
				}
				else {
					NotificationService.error(response.message);
				}
			})
			.catch(err => {
				// TODO: Jon - validation failed will show up here...
				NotificationService.error(err.message || 'Unable to save');

				// add validation stuffs...
				if (!isValid(err)) {
					this.setState({ validationResults: err.validation.results });
				}
			});
	}

	async onCancel(event) {
		const { history } = this.props;
		debug('cancel');

		history.push('/products');
	}

	render() {
		const { validationResults } = this.state;

		const validation = isNullOrEmpty(validationResults) ? undefined : (
			<ValidationResults className="mb-3" results={validationResults} />
		);

		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: (
				<ValidatorForm ref={c => { this.form = c }} onSubmit={this.onSubmit} noValidate>
					<ProductDetail
						product={this.state.product}
						productChanged={this.onProductChanged}
						ref={c => { this.productDetailForm = c }}
					/>
					{validation}
					<div className="button-container d-flex">
						<button type="submit" className="btn btn-primary ml-auto mr-2">Save</button>
						<button type="button" className="btn btn-danger" onClick={this.onCancel}>Cancel</button>
					</div>
				</ValidatorForm>
			);

		return (
			<div className="product-detail">
				<h2>Product</h2>
				{contents}
			</div>
		);
	}
}

const productWithRouter = withRouter(Product);
export { productWithRouter as Product };
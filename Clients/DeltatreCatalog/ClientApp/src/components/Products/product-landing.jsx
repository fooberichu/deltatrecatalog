import React, { Component } from 'react';
import { Switch, Route } from 'react-router';

import { ProductLayout } from './product-layout';
import { ProductListing } from './product-listing';
import { Product } from './product';

export class ProductLanding extends Component {
	static displayName = ProductLanding.name;

	render() {
		return (
			<ProductLayout>
				<Switch>
					<Route exact path='/products' component={ProductListing} />
					<Route path='/products/:productId' component={Product} />
				</Switch>
			</ProductLayout>
		)
	}
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { isNullOrUndefined } from '../../_helpers';
import { TextRequiredValidator } from '../Common/text-required-textbox';

export class ProductDetail extends Component {
	static displayName = ProductDetail.name;

	constructor(props) {
		super(props);

		this.handleTextChange = this.handleTextChange.bind(this);
	}

	handleTextChange(e) {
		let { product } = this.props;
		product[e.target.name] = e.target.value;

		this.props.productChanged(product);
	}

	render() {
		const { product } = this.props;

		const isNew = isNullOrUndefined(product.productId);

		return (
			<div>
				<div className="form-group">
					<label htmlFor="productName">Name</label>
					{/* <input type="text" className="form-control" id="productName" name="name" placeholder="Name" maxLength="250" value={product.name} onChange={this.handleTextChange} required /> */}
					<TextRequiredValidator
						className="form-control"
						id="productName"
						name="name"
						placeholder="Name"
						maxLength="250"
						value={product.name}
						onChange={this.handleTextChange}
						validators={['required']}
						errorMessages={'Name is required.'}
					/>
					<div className="error" id="nameError" />
				</div>
				<div className="form-group">
					<label htmlFor="productDescription">Description</label>
					<textarea className="form-control" id="productDescription" name="description" placeholder="Description" rows="2" value={product.description} onChange={this.handleTextChange} />
				</div>
				<div className="form-row">
					<div className="col-6 col-sm-4 col-md-2">
						<div className="form-group">
							<label htmlFor="productName">Quantity on Hand</label>
							<input type="number" className="form-control" id="productQoH" name="quantityOnHand" min="0" step="1" placeholder="0" maxLength="3" value={product.quantityOnHand} onChange={this.handleTextChange} />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ProductDetail.propTypes = {
	product: PropTypes.shape({}), // TODO: set shape
	productChanged: PropTypes.func.isRequired
}	
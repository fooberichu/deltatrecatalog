import { isNullOrUndefined } from './object-utils';

export const isValid = (response) => {
	if (isNullOrUndefined(response.validation) || response.validation.isValid)
		return true;

	return false;
};

export const verifyResponseIsValid = (response) => {
	if (isValid(response))
		return Promise.resolve(response);

	return Promise.reject(response);
};

export const required = (value) => {
	if (!value.toString().trim().length) {
		// We can return string or jsx as the 'error' prop for the validated Component
		return 'Required';
	}
};
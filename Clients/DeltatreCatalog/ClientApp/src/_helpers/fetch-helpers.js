import { isNullOrUndefined } from './object-utils';

export const VerifyResponseStatus = response => {
	// console.debug('VerifyResponseStatus response: ', response);

	// https://developer.mozilla.org/en-US/docs/Web/API/Response/ok
	if (response.ok)
		return Promise.resolve(response);

	const error = new Error(response.statusText);
	error.status = response.status;

	return Promise.reject(error)
}

export const handleResponse = (response) => {
	return response.text().then(text => {
		const data = text && JSON.parse(text);
		if (!response.ok) {
			const error = (data && data) || response.statusText;
			return Promise.reject(error);
		}

		return data;
	});
};

export const requestBase = (() => {
	if (typeof window !== 'undefined') {
		return {
			method: 'POST',
			credentials: 'same-origin',
			//body: JSON.stringify({ productId, options, quantity }),
			headers: new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' })
		};
	}
	else return {
		method: 'POST',
		credentials: 'same-origin',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	};
})();

// export const withAuthorizationHeader = (request) => {
// 	let user = localStorage.getItem('user');
// 	console.log('user', user);

// 	if (!isNullOrUndefined(user)) {
// 		user = user && JSON.parse(user);
// 		// TODO: Jon - Intranet - .append works only for Headers version for window not the object version
// 		request.headers.append('Authorization', `Bearer ${user.token}`);
// 	}

// 	return request;
// }

// export const authorizedRequestBase = (() => withAuthorizationHeader(requestBase))();
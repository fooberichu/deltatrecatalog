import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { ToastContainer } from 'react-toastify';

import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { ProductLanding } from './components/Products/product-landing';

import './_helpers/fontawesome-library-helper';

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/products' component={ProductLanding} />
        </Switch>

        <ToastContainer
          className="deltatre-toast-container"
          toastClassName="deltatre-toast"
        />
      </Layout>
    );
  }
}

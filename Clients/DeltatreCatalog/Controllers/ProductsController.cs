﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DeltatreCatalog.Logic.Constants;
using DeltatreCatalog.Logic.Extensions.ModelExtensions;
using DeltatreCatalog.Logic.Handlers;
using DeltatreCatalog.Model;
using DeltatreCatalog.Requests.Products;
using Microsoft.AspNetCore.Mvc;

namespace DeltatreCatalog.Controllers
{
	[Route("api/[controller]")]
	public class ProductsController : Controller
	{
		public ProductsController(IGetProductsHandler getHandler, 
			IUpdateProductHandler updateHandler,
			IDeleteProductHandler deleteHandler)
		{
			GetHandler = getHandler;
			UpdateHandler = updateHandler;
			DeleteHandler = deleteHandler;
		}

		protected IGetProductsHandler GetHandler { get; }
		protected IUpdateProductHandler UpdateHandler { get; }
		protected IDeleteProductHandler DeleteHandler { get; }

		// GET: api/<controller>
		[HttpGet]
		public async Task<IActionResult> Get([FromQuery]ProductRequest request)
		{
			// ideally we'd have pagination... but requirements said not required
			var products = await GetHandler.GetAsync(request);

			return Ok(products);
		}

		// GET api/<controller>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var product = await GetHandler.GetAsync(id);

			if (product == null)
				return NotFound(id);

			return Ok(product);
		}

		// POST api/<controller>
		[HttpPost]
		public async Task<IActionResult> Post([FromBody]Product product)
		{
			if (!product.IsNew())
				return BadRequest(ProductMessageConstants.Product_Error_CannotPostWithId);

			UpdateHandler.Initialize(product);
			var productResponse = await UpdateHandler.ProcessUpdateRequestAsync();

			if (productResponse.Success)
				return Ok(productResponse);

			return BadRequest(productResponse);
		}

		// PUT api/<controller>/5
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(int id, [FromBody]Product product)
		{
			UpdateHandler.Initialize(product);
			var productResponse = await UpdateHandler.ProcessUpdateRequestAsync();

			if (productResponse.Success)
				return Ok(productResponse);

			return BadRequest(productResponse);
		}

		// DELETE api/<controller>/5
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			var deleteResponse = await DeleteHandler.DeleteAsync(id);

			if (deleteResponse.Success)
				return Ok(deleteResponse);

			return BadRequest(deleteResponse);
		}
	}
}
